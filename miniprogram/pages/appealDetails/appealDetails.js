import {
  appealDetail,
  appealToEndorse,
  appealToCancel,
  appealPutComment,
  appealBrowseLog,

  communicationAndAppeal
} from "../../api/index"
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    appeal: {},
    CustomBar: app.globalData.CustomBar,
    previewImgStatus: false,
    currentIndex: 0,
    comment: '',
    // 是否显示沟通
    showCommunication: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {

    let userInfo = wx.getStorageSync('wjUser')

    let data = {
      appealId: options.appealId
    }

    // 查询诉求
    let appeal = await appealDetail(data);
    this.setData({
      appeal: appeal,
      showCommunication: userInfo ? appeal._openid != userInfo._openid : false
    })

    // 记录浏览记录
    appealBrowseLog(data)

  },


  // 为诉求点赞
  appealEndorse: async function (e) {

    let appealId = e.currentTarget.dataset.id

    let data = {
      appealId: appealId,
    }

    let result = await appealToEndorse(data);

    if (result._id) {
      let appeal = this.data.appeal

      appeal.isEndorse = true
      appeal.endorseCount = appeal.endorseCount + 1

      this.setData({
        appeal: appeal
      })
    } else {

      wx.showToast({
        title: '点赞失败',
        icon: 'none'
      })

    }



  },

  // 取消点赞
  cancelEndorse: async function (e) {


    let appealId = e.currentTarget.dataset.id
    let data = {
      appealId: appealId
    }

    let result = await appealToCancel(data);

    if (result.stats.removed > 0) {

      // 改变值
      let appeal = this.data.appeal

      appeal.isEndorse = false
      appeal.endorseCount = appeal.endorseCount - 1

      this.setData({
        appeal: appeal
      })


    } else {

      wx.showToast({
        title: '取消失败',
        icon: 'none'
      })

    }



  },

  // 监控评论
  monitorComment(e) {

    let comment = e.detail.value

    this.setData({
      comment: comment
    })

  },


  // 发送评论
  putComment: async function () {

    let comment = this.data.comment

    let newComment = comment.replace(/\s+/g, "")
    if (!newComment) {
      return
    }

    let data = {
      appealId: this.data.appeal._id,
      content: comment,
    }

    let result = await appealPutComment(data);

    if (result._id) {
      this.setData({
        comment: ''
      })
      wx.showToast({
        title: '评论成功',
        icon: "none"
      })

    }




  },


  // 去评论页面
  gotoComment() {

    let appealId = this.data.appeal._id

    wx.navigateTo({
      url: '../appealComment/appealComment?appealId=' + appealId,
    })

  },


  // 点击沟通
  linkUp: async function () {

    let data = {
      appealId: this.data.appeal._id,
      _openid: this.data.appeal._openid
    }

    console.log(data)

    await communicationAndAppeal(data)

    wx.switchTab({
      url: '/pages/message/message',
    })


  },



  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  previewImg: function () {
    // wx.previewImage({
    //   current: 'https://weiju1.oss-cn-shenzhen.aliyuncs.com/xiaochengxu-readme/channels4_banner.jpg', // 当前显示图片的http链接
    //   urls: ['https://weiju1.oss-cn-shenzhen.aliyuncs.com/xiaochengxu-readme/channels4_banner.jpg'] // 需要预览的图片http链接列表
    // })
    this.setData({
      previewImgStatus: true
    })
  },
  imgChange: function (e) {
    let index = e.detail
    this.setData({
      currentIndex: index
    })
  }
})