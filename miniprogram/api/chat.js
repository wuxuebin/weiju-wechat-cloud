//沟通诉求 (与诉求建立聊天室)
/**
 * @param {String} appealId // 诉求id
 * @param {String} _openid // 发帖人openid
 */
export const communicationAndAppeal = async function (params) {
  let data = {
    api: 'communicationAndAppeal',
    data: params
  }
  const {
    result
  } = await wx.cloud.callFunction({
    data,
    name: 'chat'
  })

  
  if (result.code != '00000') {
    wx.showToast({
      title: '沟通失败',
      icon: 'none'
    })
    return false
  }

  return result.data
}


// 查询所有的聊天室
export const queryAllChat = async function (params) {

  let data = {
    api: 'queryAllChat',
  }
  const {
    result
  } = await wx.cloud.callFunction({
    data,
    name: 'chat'
  })

  if (result.code != '00000') {
    wx.showToast({
      title: '查询失败',
      icon: 'none'
    })
    return false
  }

  return result.data
}

// 查询所有的聊天室
/**
 * @param {String} chatId // 聊天室id
 * @param {Number} pageNO // 页数
 * @param {Number} pageSize // 每页数量
 */
export const queryPageWjChatRecord = async function (params) {

  let data = {
    api: 'queryPageWjChatRecord',
    data: params
  }
  const {
    result
  } = await wx.cloud.callFunction({
    data,
    name: 'chat'
  })

  if (result.code != '00000') {
    wx.showToast({
      title: '查询失败',
      icon: 'none'
    })
    return false
  }

  return result.data
}

// 发送消息
/**
 * @param {String} chatId // 聊天室id
 * @param {String} content // 消息内容
 * @param {String} type // text、jpg ...
 * @param {String} headPortat // 头像
 */
export const sendMsg = async function (params) {

  let data = {
    api: 'sendMsg',
    data: params
  }
  const {
    result
  } = await wx.cloud.callFunction({
    data,
    name: 'chat'
  })

  if (result.code != '00000') {
    wx.showToast({
      title: '发送失败',
      icon: 'none'
    })
    return false
  }

  return result.data
}