// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()


// 查询所有诉求（限制用户沟通即可)
const queryAllChat = async function (params) {
  const wxContext = cloud.getWXContext()

  try {

    let chats = await db.collection('chat').where({
      'chatUser._openid': wxContext.OPENID,
    }).get();
    chats = chats.data

    return chats

  } catch (error) {

    console.log('appealSave error' + error)

    return []
  }

}

module.exports = queryAllChat