# 如何贡献我的源代码

此文档介绍了 **微距小程序** 的组成以及运转机制，您提交的代码将给 **微距** 项目带来什么好处，以及如何才能加入我们的行列。

## 通过 Gitee 贡献代码

**微距** 目前使用 Git 来控制程序版本，如果你想为 **微距** 贡献源代码，请先大致了解 Git 的使用方法。我们目前把项目托管在 Gitee上，任何 Gitee用户都可以向我们贡献代码。

参与的方式很简单，`fork`一份 **微距** 的代码到你的仓库中，修改后提交，并向我们发起`pull request`申请，我们会及时对代码进行审查并处理你的申请并。审查通过后，你的代码将被`merge`进我们的仓库中，这样你就会自动出现在贡献者名单里了，非常方便。

我们希望你贡献的代码符合：

- **微距**的编码规范
- 适当的注释，能让其他人读懂
- 遵循 GPL-3.0开源协议

**如果想要了解更多细节或有任何疑问，请继续阅读下面的内容**



------



### 注意事项

#### 命名规范

- 前端小程序文件名: 以首字母小写的驼峰写法

- class样式类命名

  小写并以中线连接，例如

  ```
  <view class="home-page"></view>
  ```

- js变量名

  - 基本变量的驼峰命名

    ```
    let homePage = []
    let name = '小明'
    ```

    

  - 常量名的大写或首字符大写

    ```
    const AudioContext = wx.createInnerAudioContext() 
    const API = {}
    ```



------



#### css书写规范

```css```或```wxss```的样式书写，应遵循父元素下的子元素，递进式的书写，谨慎处理公共样式

```html
<view class="home-page">
    <view class="header">
        <view class="title">标题</view>
    </view>
<view>
```

* 反例

```css
.home-page{}
.header{}
.title{}
```

* 标准

```css
.home-page{}
.home-page .header{}
.home-page .header .title{}
```

注意：对于敏感的单词，例如```header```和```title```等，一定要做特别的限制要求，容易造成样式混合错乱。开发中自己视情况而定。

------

rpx 和 px 的运用

```rpx```(手机的宽度是750rpx)使用属性：

* width
* height
* padding
* margin
* top，bottom，righit，left

```px```使用属性：

* font-size
* box-shadow
* border-radius
* border



------



#### JS处理规范



- js变量定义

  比如data 中的值，必须在 wxml 中有用，不然不放在 data 中。  其他值放入pageData中。

  ```
  Page({
      
      data: {}
      pageData:{}
  })
  ```

  

#### 小程序项目app.js的建议

* 尽量不要在app.js这个页面做一些请求或者判断跳转操作，这个容易导致开发的时候页面出不来，黑屏卡死操作
* 如果需要进入判断跳转的操作，建议加一个中转页，里面可以放一些跳转loading动画之类的



#### 目录结构规范

* components   //公共的组件封装文件夹
* assets       //静态资源文件
* pages        //所有的页面文件夹
* utils        //所有的工具函数文件夹(包括封装的ajax)
* readme.md    //项目介绍

封装工具函数的js文件统一放在```utils```文件夹



------



## Gitee Issue

Gitee 提供了 Issue 功能，该功能可以用于：

- 提出 bug
- 提出功能改进
- 反馈使用体验

该功能不应该用于：

- 提出修改意见（涉及代码署名和修订追溯问题）
- 不友善的言论

## 快速修改

**Gitee 提供了快速编辑文件的功能**

1. 登录 Gitee 帐号；
2. 浏览项目文件，找到要进行修改的文件；
3. 点击右上角铅笔图标进行修改；
4. 填写 `Commit changes` 相关内容（Title 必填）；
5. 提交修改，等待 CI 验证和管理员合并。

**若您需要一次提交大量修改，请继续阅读下面的内容**

## 完整流程

1. `fork`本项目；
2. 克隆(`clone`)你 `fork` 的项目到本地；
3. 新建分支(`branch`)并检出(`checkout`)新分支；
4. 添加本项目到你的本地 git 仓库作为上游(`upstream`)；
5. 变基（衍合 `rebase`）你的分支到上游 master 分支；
6. `push` 你的本地仓库到 Gitee；
7. 提交 `pull request`；
8. 等待 CI 验证（若不通过则重复 5~7，Gitee 会自动更新你的 `pull request`）；
9. 等待管理员处理，并及时 `rebase` 你的分支到上游 master 分支（若上游 master 分支有修改）。

*若有必要，可以 `git push -f` 强行推送 rebase 后的分支到自己的 `fork`*

*绝对不可以使用 `git push -f` 强行推送修改到上游*

## 注意事项

- 若对上述流程有任何不清楚的地方，请查阅 GIT 教程，如 [这个](http://backlogtool.com/git-guide/cn/)；
- 对于代码**不同方面**的修改，请在自己 `fork` 的项目中**创建不同的分支**（原因参见`完整流程`第9条备注部分）；
- 变基及交互式变基操作参见 [Git 交互式变基](http://pakchoi.me/2015/03/17/git-interactive-rebase/)